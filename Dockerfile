FROM golang AS builder

COPY fullcycle.go .

RUN go build fullcycle.go

FROM scratch

COPY --from=builder /go/fullcycle .

ENTRYPOINT ["./fullcycle"]